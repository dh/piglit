# Test that varying structs work properly.
#
# From the GLSL 1.50 specification, section 4.3.4 ("Input Variables"):
#
# "Fragment inputs can only be signed and unsigned integers and
#  integer vectors, float, floating-point vectors, matrices, or
#  arrays or structures of these."
#
# And from section 4.3.6 ("Output Variables"):
#
# "Vertex and geometry output variables output per-vertex data and are
#  declared using the out storage qualifier, the centroid out storage
#  qualifier, or the deprecated varying storage qualifier. They can only be
#  float, floating-point vectors, matrices, signed or unsigned integers or
#  integer vectors, or arrays or structures of any these."
#
# This test verifies basic functionality of varying structs using a
# varying struct containing a variety of types.

[require]
GL >= 3.2
GLSL >= 1.50

[vertex shader]
#version 150

uniform float ref;
in vec4 vertex;
out vec4 pos;

struct Foo
{
	mat4 a;
	mat3 b;
	mat2 c;
	vec4 d;
	vec3 e;
	vec2 f;
	float g;
};
out Foo foo;

void main()
{
	gl_Position = vertex;
	pos = vertex;

        // introduce a fake dependency on inputs to prevent the compiler from
        // moving ref to the next shader
        float refx = vertex.x < -10.0 ? 0.0 : ref;

	foo.a = mat4(refx,        refx +  1.0, refx +  2.0, refx +  3.0,
		     refx +  4.0, refx +  5.0, refx +  6.0, refx +  7.0,
		     refx +  8.0, refx +  9.0, refx + 10.0, refx + 11.0,
		     refx + 12.0, refx + 13.0, refx + 14.0, refx + 15.0);

	foo.b = mat3(refx + 16.0, refx + 17.0, refx + 18.0,
		     refx + 19.0, refx + 20.0, refx + 21.0,
		     refx + 22.0, refx + 23.0, refx + 24.0);

	foo.c = mat2(refx + 25.0, refx + 26.0,
		     refx + 27.0, refx + 28.0);

	foo.d = vec4(refx + 29.0, refx + 30.0, refx + 31.0, refx + 32.0);
	foo.e = vec3(refx + 33.0, refx + 34.0, refx + 35.0);
	foo.f = vec2(refx + 36.0, refx + 37.0);
	foo.g = refx + 38.0;
}

[geometry shader]
#version 150

uniform float ref;

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec4 pos[];

flat out int gs_pass;

struct Foo
{
	mat4 a;
	mat3 b;
	mat2 c;
	vec4 d;
	vec3 e;
	vec2 f;
	float g;
};
in Foo foo[];

#define CHECK(value, expected) \
	if (distance(value, expected) > 0.00001) \
		pass = false

void main()
{
	bool pass = true;

	for(int i = 0; i < 3; i++) {
		CHECK(foo[i].a[0], vec4(ref,        ref +  1.0, ref +  2.0, ref +  3.0));
		CHECK(foo[i].a[1], vec4(ref +  4.0, ref +  5.0, ref +  6.0, ref +  7.0));
		CHECK(foo[i].a[2], vec4(ref +  8.0, ref +  9.0, ref + 10.0, ref + 11.0));
		CHECK(foo[i].a[3], vec4(ref + 12.0, ref + 13.0, ref + 14.0, ref + 15.0));

		CHECK(foo[i].b[0], vec3(ref + 16.0, ref + 17.0, ref + 18.0));
		CHECK(foo[i].b[1], vec3(ref + 19.0, ref + 20.0, ref + 21.0));
		CHECK(foo[i].b[2], vec3(ref + 22.0, ref + 23.0, ref + 24.0));

		CHECK(foo[i].c[0], vec2(ref + 25.0, ref + 26.0));
		CHECK(foo[i].c[1], vec2(ref + 27.0, ref + 28.0));

		CHECK(foo[i].d, vec4(ref + 29.0, ref + 30.0, ref + 31.0, ref + 32.0));
		CHECK(foo[i].e, vec3(ref + 33.0, ref + 34.0, ref + 35.0));
		CHECK(foo[i].f, vec2(ref + 36.0, ref + 37.0));
		CHECK(foo[i].g, ref + 38.0);
	}

	for(int i = 0; i < 3; i++) {
		gl_Position = pos[i];
		gs_pass = int(pass);
		EmitVertex();
	}
}

[fragment shader]
#version 150

flat in int gs_pass;
out vec4 color;

void main()
{
	if (gs_pass == 0)
		color = vec4(1.0, 0.0, 0.0, 1.0);
	else
		color = vec4(0.0, 1.0, 0.0, 1.0);
}

[vertex data]
vertex/float/2
-1.0 -1.0
 1.0 -1.0
 1.0  1.0
-1.0  1.0

[test]
uniform float ref 137.035999074
draw arrays GL_TRIANGLE_FAN 0 4
probe all rgba 0.0 1.0 0.0 1.0
