include_directories(
	${GLEXT_INCLUDE_DIR}
	${OPENGL_INCLUDE_PATH}
)

link_libraries (
	piglitutil_${piglit_target_api}
	${OPENGL_gl_LIBRARY}
)

piglit_add_executable (ext_debug_label-object-label_${piglit_target_api} debug-object-label.c)

# vim: ft=cmake:
