piglit (0~git20230531-5036601c4-1) UNRELEASED; urgency=medium

  * Non-maintainer upload.

 -- David Heidelberg <david@ixit.cz>  Wed, 31 May 2023 22:43:26 +0200

piglit (0~git20220119-124bca3c9-1) unstable; urgency=medium

  * Update piglit upstream version to 124bca3c9 (Closes: #1002369)
  * d/control: Add vulkan build dependencies
  * debian: Convert to cmake ninja build
  * d/control: Set Standards-Version to 4.6.0.1
  * d/control: Set debhelper-compat to 13
  * debian: Remove old lintian overrides
  * d/rules: Set DEB_BUILD_MAINT_OPTIONS = hardening=+all for lintian hardening-no-bindnow
  * d/piglit.lintian-overrides: Skip package-contains-documentation-outside-usr-share-doc for asmparsertest tests
  * d/piglit-dbgsym.lintian-overrides: Skip elf-error for debug symbol files
  * d/control: Update email address for Jordan Justen

 -- Jordan Justen <jljusten@debian.org>  Wed, 19 Jan 2022 23:32:20 -0800

piglit (0~git20200212-f4710c51b-1) unstable; urgency=medium

  * Update piglit upstream version to f4710c51b (Closes: #933275,
    #948284)
  * d/gbp.conf: Fix gbp config file duplicate entry
  * d/control: Recommend waffle-utils (Closes: #943799)
  * d/control: Depend on bash-completion (Closes: #943802)
  * d/rules: Remove gcc-7 workaround (Closes: #922498)
  * d/changelog: Note that #933275 was fixed by piglit upgrade
  * d/changelog: Note that #948284 was fixed by piglit upgrade
  * d/control: Update Standards-Version to 4.4.1
  * debian: Update to debhelper-compat 12
  * d/copyright: Remove two deleted files
  * d/control: Update Standards-Version to 4.5.0
  * d/rules: Fix lintian package-contains-empty-directory
  * d/patches: Fix bash completion install and lintian warning

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 21 Feb 2020 21:20:04 -0800

piglit (0~git20180515-62ef6b0db-1) unstable; urgency=low

  * Update piglit upstream version to 62ef6b0db
  * Add dh-python build dependency for missing-build-dependency-for-dh-addon
    lintian error, closes: #896801
  * Update to debhelper 11
  * Update Standards-Version to 4.1.4
  * Remove unused lintian override binary-without-manpage
  * Remove unused lintian override space-in-std-shortname-in-dep5-copyright
  * Remove --parallel for debian-rules-uses-unnecessary-dh-argument
    lintian warning
  * Change Vcs-* to use https on salsa.debian.org for
    lintian vcs-field-uses-insecure-uri
  * Fix lintian insecure-copyright-format-uri

 -- Jordan Justen <jordan.l.justen@intel.com>  Wed, 16 May 2018 02:50:49 -0700

piglit (0~git20170210-508210dc1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Dependency fixes:
    - remove ${misc:Pre-Depends} from Depends
    - add ${python3:Depends} instead of manual python3
    - add python3-mako (Closes: #830145)
  * Workaround gcc-7 memory usage regression by building
    with -O1 on i386.

 -- Adrian Bunk <bunk@debian.org>  Sat, 14 Oct 2017 18:26:57 +0300

piglit (0~git20170210-508210dc1-1) unstable; urgency=low

  * Update piglit upstream version to 508210dc1
  * Use Python 3
  * Update standards version to 3.9.8

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 10 Feb 2017 13:51:50 -0800

piglit (0~git20150829-59d7066-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build OpenCL tests (Closes: #845549)
  * Build in parallel (Closes: #846403)

 -- Simon Richter <sjr@debian.org>  Thu, 26 Jan 2017 18:34:58 +0100

piglit (0~git20150829-59d7066-1) unstable; urgency=low

  * Update piglit version
  * Update standards version to 3.9.6
  * Add python-six as a dependency
  * Add debian copyright entries
  * Add libgbm-dev build dependency
  * Add new debian packaging, closes: #761015

 -- Jordan Justen <jordan.l.justen@intel.com>  Sat, 29 Aug 2015 12:55:28 -0700

piglit (0~git20140926-4efb025-1) unstable; urgency=low

  * Add new debian packaging, closes: #761015

 -- Jordan Justen <jordan.l.justen@intel.com>  Sat, 27 Sep 2014 07:52:33 -0700
