#!/usr/bin/python3

import os
import sys

multiarch = os.environ['DEB_HOST_MULTIARCH']

assert len(sys.argv) == 2
f = open(sys.argv[1], 'rt', encoding="utf-8")
s = f.read()
f.close()

r = s.replace("'lib64'", "'lib/%s', 'lib64'" % multiarch)
assert len(r) > len(s)

f = open(sys.argv[1], 'wt', encoding="utf-8")
f.write(r)
f.close()

sys.exit(0)
